import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'postcode',
    method: 'get',
    params: query
  })
}

export function fetchPostcode(id) {
  return request({
    url: '/vue-element-admin/Postcode/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/Postcode/pv',
    method: 'get',
    params: { pv }
  })
}

export function createPostcode(data) {
  return request({
    url: 'postcode',
    method: 'post',
    data
  })
}

export function updatePostcode(data) {
  return request({
    url: 'postcode/'+data.id,
    method: 'put',
    data
  })
}

export function deletePostcode(id) {
  return request({
    url: 'postcode/'+id,
    method: 'delete',
  })
}
