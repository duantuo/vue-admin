import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'addon',
    method: 'get',
    params: query
  })
}

export function fetchAddon(id) {
  return request({
    url: '/vue-element-admin/Addon/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/Addon/pv',
    method: 'get',
    params: { pv }
  })
}

export function createAddon(data) {
  return request({
    url: 'addon',
    method: 'post',
    data
  })
}

export function updateAddon(data) {
  return request({
    url: 'addon/'+data.id,
    method: 'put',
    data
  })
}

export function deleteAddon(id) {
  return request({
    url: 'addon/'+id,
    method: 'delete',
  })
}

export function subscriptionAddon(data) {
  return request({
    url: 'subscription/addon',
    method: 'post',
    data
  })
}