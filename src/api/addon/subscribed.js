import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'subscription/addon',
    method: 'get',
    params: query
  })
}

export function deleteAddon(id) {
  return request({
    url: 'unsubscription/addon/'+id,
    method: 'delete',
  })
}
