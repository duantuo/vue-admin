import request from '@/utils/request'

export function fetchList(data) {
  return request({
    url: 'trackings',
    method: 'post',
    data
  })
}