import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'getMenu',
    method: 'get',
    params: query
  })
}

export function fetchMenu(id) {
  return request({
    url: '/vue-element-admin/Menu/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/Menu/pv',
    method: 'get',
    params: { pv }
  })
}

export function createMenu(data) {
  return request({
    url: 'createMenu',
    method: 'post',
    data
  })
}

export function updateMenu(data) {
  return request({
    url: 'updateMenu/'+data.id,
    method: 'put',
    data
  })
}

export function updateVisible(data) {
  return request({
    url: 'updateVisible/menu',
    method: 'put',
    data
  })
}

export function deleteMenu(id) {
  return request({
    url: 'deleteMenu/'+id,
    method: 'delete',
  })
}

export function subscriptionMenu(data) {
  return request({
    url: 'subscription/addon',
    method: 'post',
    data
  })
}

