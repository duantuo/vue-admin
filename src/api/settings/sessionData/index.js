import request from '@/utils/request'

export function getSessionData(data) {
  return request({
    url: 'getSessionData',
    method: 'post',
    data
  })
}
