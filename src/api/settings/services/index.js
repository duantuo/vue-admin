import request from '@/utils/request'

export function getList(params) {
  return request({
    url: 'services',
    method: 'get',
    params
  })
}
