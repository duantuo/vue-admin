import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'customer',
    method: 'get',
    params: query
  })
}

export function createCustomer(data) {
  return request({
    url: 'customer',
    method: 'post',
    data
  })
}

export function updateCustomer(data) {
  return request({
    url: 'customer/'+data.id,
    method: 'put',
    data
  })
}

export function deleteCustomer(id) {
  return request({
    url: 'customer/'+id,
    method: 'delete',
  })
}