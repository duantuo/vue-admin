import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'account',
    method: 'get',
    params: query
  })
}

export function fetchAccount(id) {
  return request({
    url: '/vue-element-admin/Account/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/Account/pv',
    method: 'get',
    params: { pv }
  })
}

export function createAccount(data) {
  return request({
    url: 'account',
    method: 'post',
    data
  })
}

export function updateAccount(data) {
  return request({
    url: 'account/'+data.id,
    method: 'put',
    data
  })
}

export function deleteAccount(id) {
  return request({
    url: 'account/'+id,
    method: 'delete',
  })
}

export function subscribedPlugin(id) {
  return request({
    url: 'plugins/account/'+id,
    method: 'get',
  })
}

export function getAccountUser(id) {
  return request({
    url: 'accountUser/'+id,
    method: 'get',
  })
}

export function switchAccount(data) {
  return request({
    url: 'switchAccount',
    method: 'post',
    data
  })
}

export function accountStore(id) {
  return request({
    url: 'store/accountStores/'+id,
    method: 'get',
  })
}