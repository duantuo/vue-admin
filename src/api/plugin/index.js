import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: 'plugin',
    method: 'get',
    params: query
  })
}

export function fetchPlugin(id) {
  return request({
    url: '/vue-element-admin/Plugin/detail',
    method: 'get',
    params: { id }
  })
}

export function fetchPv(pv) {
  return request({
    url: '/vue-element-admin/Plugin/pv',
    method: 'get',
    params: { pv }
  })
}

export function createPlugin(data) {
  return request({
    url: 'plugin',
    method: 'post',
    data
  })
}

export function updatePlugin(data) {
  return request({
    url: 'plugin/'+data.id,
    method: 'put',
    data
  })
}

export function deletePlugin(id) {
  return request({
    url: 'plugin/'+id,
    method: 'delete',
  })
}

export function subscriptionPlugin(data) {
  return request({
    url: 'subscription/plugin',
    method: 'post',
    data
  })
}

export function updateVisible(data) {
  return request({
    url: 'updateVisible/plugin',
    method: 'put',
    data
  })
}