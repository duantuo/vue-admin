import Cookies from 'js-cookie'

const TokenKey = 'access_token'
const userInfoKey = 'user_info'

export function getToken() {
  return Cookies.get(TokenKey)
}

export function setToken(token) {
  return Cookies.set(TokenKey, token)
}

export function removeToken() {
  return Cookies.remove(TokenKey)
}

export function getSession() {
  return JSON.parse(sessionStorage.getItem(userInfoKey))
}

export function setSession(data) {
  return sessionStorage.setItem(userInfoKey, data)
}

export function removeSession() {
  return sessionStorage.removeItem(userInfoKey)
}